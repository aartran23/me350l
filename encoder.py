# -*- coding: utf-8 -*-
'''@file        Encoder.py
@brief          Keeps track of motor position. This class and its methods are intended to be used and called by CTRL_task.py.
@details        This class makes use of timers available on the Nucleo development board to incrementally update the count of the encoder. T
                This class interprets the timer count and converts it into an angular position value of the encoder. Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/encoder.py
@author         Aaron Tran
@date           3/17/21
'''

import utime

class mot_Enc:
    '''Implements a motor encoder class for the ME405 board.
    '''
    def __init__(self, pin1, pin2, timer):
        '''@param pin1 one of two pins that physical encoder is connected to
        @param pin2 one of two pins that physical encoder is connected to
        @param timer timer corresponding to two pins used for physical encoder
        '''
        ##@brief one of two pins that physical encoder is connected to
        self.pin1 = pin1
        ##@brief one of two pins that physical encoder is connected to     
        self.pin2 = pin2
        ##@brief timer corresponding to two pins used for physical encoder
        self.timer = timer
        
        ##@brief timer channel to enable counting in one direction
        self.ch1 = self.timer.channel(1,self.timer.ENC_AB, pin=self.pin1)
        ##@brief timer channel to enable counting in another direction
        self.ch2 = self.timer.channel(2,self.timer.ENC_AB, pin=self.pin2)
        ##@brief current time of system
        self.thisTime = utime.ticks_ms()
        ##@brief current count of encoder
        self.ctNow = self.timer.counter()
        ##@brief position of encoder
        self.theta = 0
        ##@brief most recent change in encoder position
        self.delta = 0

    def run(self):
        '''@brief Runs to constantly update motor position on 20ms interval
        '''
        ##@brief global time of system
        self.globT = utime.ticks_ms()
        if utime.ticks_diff(self.globT,self.thisTime) >= 20:
            self.update()
            self.thisTime = utime.ticks_ms()
                
    def update(self):
        '''@brief updates recorded position of encoder
        '''
        ##@brief uncorrected delta value
        self.delta_check = self.timer.counter() - self.ctNow    
        # self.delta = self.tim4.counter() - self.ctNow    
        if self.delta_check > -0xFFFF/2 and self.delta_check < 0xFFFF/2:
            pass
        elif self.delta_check >= 0xFFFF/2:
            self.delta_check -= 0xFFFF   
        elif self.delta_check <= -0xFFFF/2:
            self.delta_check += 0xFFFF       
        pass    
        self.delta = self.delta_check*360/4000    
        self.theta += self.delta
        self.ctNow = self.timer.counter()

    def get_position(self):
        '''@brief returns most recently updated position of encoder
        '''
        return(self.theta)
    
    def set_position(self,pos):
        '''@brief resets position to specified value
        @param pos desired position to set encoder value to
        '''
        self.theta = pos
    
    def get_delta(self):
        '''@brief returns difference in recorded position between two most recent calls to update
        '''
        return(self.delta)
        
