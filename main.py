# -*- coding: utf-8 -*-
'''@file        main.py
@brief          responsible for running UI_task.py and CTRL_task.py
@details        Runs UI_task.py and CTRL_task.py repeatedly. Will run on Nucleo with no reason to break 
                out of the loop besides turning off the machine. 
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/main.py
@author         Aaron Tran
@date           3/17/21
'''
import pyb
from pyb import UART
from CTRL_task import CTRL_task
from UI_task import UI_task
import shares

# motor = int((input("Select which motor to run (1 or 2): ")))
##@brief active motor
motor = 2
shares.motor = motor

##@brief selected UART port
myuart = UART(2)
pyb.repl_uart(None)
##@brief CTRL_task.CTRL_task object running in main.py
CTRL = CTRL_task(motor)
##@brief UI_task.UI_task object running in main.py
UI = UI_task()
while True:    
    try:    
        UI.run()
        CTRL.run()
    except KeyboardInterrupt:
        if motor == 1: 
            CTRL.mot1.disable()
        elif motor == 2:
            CTRL.mot2.disable()
        break