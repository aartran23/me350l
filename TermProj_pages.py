## \mainpage ME305 Term Project
#
# Some general info.
#
# This manual is divided in the following sections:
# - \subpage intro
# - \subpage Week1 Week 1: Extending Your Interface
# - \subpage Week2 Week 2: Incremental Encoders
# - \subpage Week3 Week 3: DC Motor Control
# - \subpage Week4 Week 4: Reference Tracking
#
# 

## \page intro Introduction
#  This project was completed with the intention of familiarizing myself with micropython.
#  I implemented a control system, which can be found in CTRL.py, to modulate motor control according to 
#  desired motor speeds and angular positions. Additionally, data about the motor control was sent to a computer
#  from the microcontroller, using serial communication techiques, specifically UART. Further detail into the development and
#  different portions of this code will be discussed more in these subpages:\n
# \ref Week1 "Week 1: Extending Your Interface"\n
# \ref Week2 "Week 2: Incremental Encoders"\n
# \ref Week3 "Week 3: DC Motor Control"\n
# \ref Week4 "Week 4: Reference Tracking"\n
#
#  The hardware used include but are not limited to:\n
#  Nucleo L476RG for the microcontroller\n
#  Two DCX22S motors\n
#  Two 5V US Digital Encoders
#
#  An image of the hardware setup is available in Figure 1 below. 
## \image html hardware_setup.jpg "Figure 1. Hardware Setup"
#   


## \page Week1 Week 1: Extending Your Interface
# This page describes work relevant to the ME305 Lab Project
# Make sure you have first read \ref intro "the introduction".
#
# In the first week, we were interested in building a user interface to facilitate bidirectional
# communication between our computers and the Nucleo board. In the interest of doing this,
# I have created a UI frontend and a data collection task. Refer to FrontEnd.py for the UI frontend. The data collection
# task is in UI_task.py, which builds an array for each set of data we are interested and then uses UART for serial communication
# to send the arrays to the computer. In the interest of improving UI on the frontend, a list of commands appears in the console window when
# running FrontEnd.py. This output is shown in Figure 1.
#
## \image html UI_frontend.png "Figure 1. UI Frontend. This output shows up on the console window, pressing appropriate keys will produce corresponding results"
# 
# The frontend will also generate a .CSV file containing the relevant data. an example of this is visible in Figure 2 below.
# \image html labdata.png "Figure 2. Motor 2 Data"

## \page Week2 Week 2: Incremental Encoders
# This page describes work relevant to the ME305 Lab Project
# Make sure you have first read \ref intro "the introduction".
#
# In this week, I developed the encoder , which can be seen in Encoder.mot_Enc. The purpose of this portion is to
# be able to track the position of the encoder through usage. To accomplish this, timers on the Nucleo were used to read pulses from encoders
# and count distance direction of motion. To account for counter overflow and underflow, correction methods were implemented of the following nature:\n
# Case 1: There is no over flow or underflow.\n
# Solution: Do nothing. The change in count is good\n
# Case 2: There is overflow\n
# Solution: Subtract half of the counter period from the change in count\n
# Case 3: There is underflow\n
# Solution: Add half of the counter period to the change in count
#
# With these three cases in mind, I accumulated the changes in count to have a trackable encoder position. With a rate of 4000 pulses per revolution, I was able to convert this into a value in degrees.

## \page Week3 Week 3: DC Motor Control
# This page describes work relevant to the ME305 Lab Project
# Make sure you have first read \ref intro "the introduction".
#
# In this week, I developed a closed loop controller class and a motor driver class. The closed loop controller class, CTRL.ClosedLoop, acts to modulate the PWM of the motor in order to achieve desired positional and speed control.
# The motor driver class, MOT.MotorDriver receives the PWM value from the CTRL.ClosedLoop and sends it to the motor. It is directly responsible for motor control and can also turn on and off the motor.
# Additionally, a new class, CTRL_task.CTRL_task, was created to call the \ref MOT.MotorDriver "motor driver class", \ref Encoder.mot_Enc "encoder class", and \ref CTRL.ClosedLoop "controller class". This makes use of each class file to run the motor and handle overall motor control.
# This is used in conjunction with CTRL_task.CTRL_task for the backend of this system, where CTRL_task.py is responsible for communicating with FrontEnd.py. The task diagram for this structure is available
# in Figure 1 below.
## \image html task_dia.jpg "Figure 1. Task diagram of complete backend structure"
#
# In order to properly set up this code, notice that CTRL.py will send a PWM value to the CTRL_task.py which will then send that same PWM value to the MOT.py. This PWM is calculated based off of the positional and speed error that the controller 
# finds. The method through which this PWM is determined is outlined in the hand calculations in Figure 2 below. Note that I refer to PWM as L in the calculations.
## \image html ctrl1.jpg
## \image html ctrl2.jpg
## \image html ctrl3.jpg
## \image html ctrl4.jpg  "Figure 2. This shows the hand calculations performed to analytically derive suitable proportional and integral gain for the controller"

## \page Week4 Week 4: Reference Tracking
# This page describes work relevant to the ME305 Lab Project
# Make sure you have first read \ref intro "the introduction".
#
# Finally, we want to drive the encoder at specific speeds and positions. To do this, we have provided the encoder with reference values to try and reach. 
# Due to real world effects, mainly a nonuniform pulley, modeling the motor and encoder properly to get a good proportional gain, Kp, and a good integral gain, Ki is difficult. 
# Ideally, we can tune these parameters, but there were some hardware difficulties that appeared during this week. Both encoders would malfunction after brief periods of time. After spinning the motors for a long enough period,
# the encoders would stop reading values properly. It would not detect any movement, despite the motor clearly moving. Additionally, motors would randomly fail, where despite being fed 100% PWM, the motor would give a beep and a small twitch.
# However, the baseline code that has been developed so far should function properly with working hardware and would allow ofr tuning in the future. As is, however, the plots we obtain are as shown below:\n
## \image html Motor2_angular_speed.png "Figure 1. Comparison between speed calculated from encoder reading and reference speed values in RPM"
## \image html Motor2_position.png "Figure 2. Comparison between encoder reading and reference position values in degrees"
# From the set of data obtained from Figures 1 and 2, we obtained an overall error value of 25826907.10639148, where the error is defined by:\n
## \image html err_fcn.png "Figure 3. Overall error value equation where omega refers to angular speed, theta refers to angular position, subscript ref refers to reference values, subscript meas refers to encoder values, k represents index of iterations, and K represnts the total number of iterations"
# With working hardware, we would want to reduce this by changing the Kp and Ki values, but the faulty equipment prevents us from doing so.

