# -*- coding: utf-8 -*-
'''@file        CTRL_task.py
@brief          Use to run motor driver, encoder, and controller together. This is intended to be called by main.py.
@details        This class runs the motor driver, encoder, and controler. It is responsible for all motor actions. Must be used in conjunction with 
                UI_task.py for full functionality. To do this, both classes have to be implemented in main.py together.
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/CTRL_task.py
@author         Aaron Tran
@date           3/17/21
'''

from CTRL import ClosedLoop
from MOT import MotorDriver 
from Encoder import mot_Enc
from array import array
import utime
import pyb
import shares 


class CTRL_task:
    '''@brief Responsible for collecting data on and controlling motor speed and position. Intended to be ran in conjunction with UI_task class and shares.py.
    '''
    def __init__(self,motor):
        '''@brief sets up hardware connections to run motor
        @details creates encoder, motor driver, and controller object for selected motor, also retrieves reference position and reference speed
        @param motor motor that user wants to run
        '''
        ##@brief motor selected
        self.motor = motor
        shares.motor = motor
        ##@brief sleep/enable pin
        self.EN_pin = pyb.Pin.cpu.A15
        ##@brief timer used by motors
        self.tim3 = pyb.Timer(3, freq=20000)
        if motor == 1:
            #make hardware objects for encoder
            ##@brief pinB6 object for motor 1 encoder
            self.pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
            ##@brief pinB7 object for motor 1 encoder
            self.pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
            ##@brief timer object for motor 1 encoder
            self.tim4 = pyb.Timer(4, prescaler=0, period = 0xFFFF)            
            ##@brief encoder driver for motor 1
            self.enc1 = mot_Enc(self.pinB6, self.pinB7, self.tim4)    
      
            #make hardware objects for motor
            ##@brief pinB4 object for motor 1 driver
            self.pinB4 = pyb.Pin.cpu.B4
            ##@brief pinB5 object for motor 1 driver
            self.pinB5 = pyb.Pin.cpu.B5
            #make driver controller object
            ##@brief Motor 1 driver
            self.mot1 = MotorDriver(self.EN_pin,self.pinB4,self.pinB5,self.tim3)
            ##@brief controller for motor 1
            self.CTRL1 = ClosedLoop()
        elif motor == 2:
            ##@brief pinC6 object for motor 2 encoder
            self.pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
            ##@brief pinC7 object for motor 2 encoder
            self.pinC7 = pyb.Pin(pyb.Pin.cpu.C7)     
            ##@brief timer object for motor 2 encoder
            self.tim8 = pyb.Timer(8, prescaler=0, period = 0xFFFF)
            ##@brief encoder driver for motor 2
            self.enc2 = mot_Enc(self.pinC6, self.pinC7, self.tim8)     
        
            ##@brief pinB0 object for motor 2 driver
            self.pinB0 = pyb.Pin.cpu.B0
            ##@brief pinB1 object for motor 2 driver
            self.pinB1 = pyb.Pin.cpu.B1        
            ##@brief motor 2 driver
            self.mot2 = MotorDriver(self.EN_pin,self.pinB0,self.pinB1,self.tim3)
            ##@brief controller for motor 2
            self.CTRL2 = ClosedLoop()      
        ##@brief list index of reference arrays
        self.n = 0
        ##@brief reference time array
        self.time_ref  = array('f', 751*[0])
        ##@brief reference speed array
        self.omega_ref  = array('f', 751*[0])
        ##@brief reference position array
        self.theta_ref  = array('f', 751*[0])
        with open('reference.csv', 'r') as csv_file:
            i=0
            try:
                for line in csv_file:
                    ##@brief string containing CSV data of reference values
                    line_Str=csv_file.readline()
                    line_Str=line_Str.rstrip('\n')
                    line_Str=line_Str.rstrip('\r')
                    line_Str=line_Str.rsplit(',')
                    self.time_ref[i] = float(line_Str[0])
                    self.omega_ref[i] = float(line_Str[1])
                    self.theta_ref[i] = float(line_Str[2])
                    i+=1
            except:
                pass
            
        self.thisTime  = utime.ticks_ms()
        ##@brief pause/play state of motor
        self.pause = True

        print("Controller Task Initialized")
        
    def run(self):
        '''@brief actuates motor, receives and sends encoder data
        @details actuates motor, compares encoder signals to reference values to modulate motor speed, reads from encoder data and sends it to UI_task.UI_task
        '''
        if utime.ticks_diff(utime.ticks_ms(), self.thisTime)>= 20 and not self.pause:
            self.n+=1
            self.thisTime = utime.ticks_ms()
            if self.n >= 751:
                self.n = 0
        if self.motor == 1:
            self.CTRL1.Kp = shares.Kp
            ##@brief motor 1 measured speed [RPM]
            self.omega_meas1 = 60*self.enc1.get_delta()/0.020/360
            self.CTRL1.run(self.omega_ref[self.n],self.omega_meas1, self.theta_ref[self.n], self.enc1.get_position())   
            self.mot1.set_duty(self.CTRL1.PWM)
            self.enc1.run()
            shares.enc1_err = (self.omega_ref[self.n] - self.omega_meas1)**2 +  (self.theta_ref[self.n] - self.enc1.get_position())**2           
            shares.enc1_pos = self.enc1.get_position()
            shares.enc1_speed = self.omega_meas1            
        elif self.motor == 2:
            self.CTRL2.Kp = shares.Kp 
            ##@brief motor 2 measured speed [RPM] 
            self.omega_meas2 = 60*self.enc2.get_delta()/0.020/360
            self.CTRL2.run(self.omega_ref[self.n],self.omega_meas2, self.theta_ref[self.n], self.enc2.get_position())    
            self.mot2.set_duty(self.CTRL2.PWM)        
            self.enc2.run()
            shares.enc2_err = (self.omega_ref[self.n] - self.omega_meas2)**2 +  (self.theta_ref[self.n] - self.enc2.get_position())**2           
            shares.enc2_pos = self.enc2.get_position()        
            shares.enc2_speed = self.omega_meas2
        
        if shares.zero_enc:
            if self.motor == 1:
                self.enc1.set_position(0)
            elif self.motor ==2:
                self.enc2.set_position(0)
            shares.zero_enc = False
        if shares.get_delta:
            if self.motor == 1:
                shares.enc1_delta = self.enc1.get_delta()
            elif self.motor == 2:
                shares.enc2_delta = self.enc2.get_delta()
            shares.get_delta = False
            shares.UI_delta = True
        if shares.kill:
            if self.motor == 1:
                self.mot1.disable()
            elif self.motor == 2:
                self.mot2.disable()
            self.pause = True
            shares.kill = False
        if shares.revive:
            if self.motor == 1:
                self.mot1.enable()
            elif self.motor == 2:
                self.mot2.enable()
            shares.revive = False
            self.pause= False