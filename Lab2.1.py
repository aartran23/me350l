# -*- coding: utf-8 -*-
'''@file        Lab2.1.py
@brief          Cycle though LED blinking patterns
@details        Implements a finite state machine to cycle through LED
                blinking patterns: Sinewave, Sawtooth, and Squarewave; Set to
                blink the LD2 LED on the Nucleo-L476RG; Source code can be found 
                at https://bitbucket.org/aartran23/me350l/src/master/Lab_2.1.py
@image          html L2STD.png "State Transition Diagram" 

@author         Aaron Tran
@date           1/27/21
'''
import utime
import pyb
import math

def Button(IRQ_src):
    '''@brief Interrupt callback function; disables running wave forms
       @param IRQ_src Interrupt argument
    '''
    global nextstate
    nextstate = True
    

if __name__ == "__main__":
    ##@brief    Maps pin C13 to variable
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    ##@brief    Maps pin A5 to variable
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
    ##@brief    Sets PWM of LED
    tim2 = pyb.Timer(2, freq = 20000)
    ##@brief    Gives pin A5 PWM value to run at
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    ##@brief    Assigns interrupt callback to button
    ButtBreak = pyb.ExtInt(pinC13, mode = pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=Button)
    ##@brief    Tells Finite State Machine what state we are in
    state = 0
    ##@brief    Will tell Finite State Machine to go to the next state if true    
    nextstate = False
    while True:
        try:
            if state==0:
                print('Greetings! To cycle through LED patterns press the B1 button')
                while not nextstate: 
                    pass
                state=1
                nextstate = False
                print('Sine Wave Selected')
                ##      Initial time for state 1
                sinetime = utime.ticks_ms()
            elif state==1:
                ##      Retrieves global ticker
                glob_time = utime.ticks_ms()
                ##      Current state timer
                t = utime.ticks_diff(glob_time, sinetime)/1000
                ##      Brightness level for sinewave form
                sinewave = 100*(0.5*math.sin(math.pi/5*t)+0.5)
                t2ch1.pulse_width_percent(sinewave)
                if nextstate:
                    state =2
                    ##      Initial time for State 2      
                    saw_time = utime.ticks_ms()
                    nextstate = False
                    print('Sawtooth Wave Selected')

            elif state==2:
                glob_time = utime.ticks_ms()
                t = utime.ticks_diff(glob_time, saw_time)/1000
                ##      Brightness level for sawtooth form
                sawtooth = 100*(t%1)
                t2ch1.pulse_width_percent(sawtooth)
                if nextstate:
                    state =3
                    ##      Initial time for State 3
                    square_time = utime.ticks_ms()
                    nextstate = False
                    print('Square Wave Selected')

            elif state==3:
                glob_time = utime.ticks_ms()
                t = utime.ticks_diff(glob_time, square_time)/1000
                ##      Brightness level for squarewave form
                squarewave = math.sin(2*math.pi*t)
                if  squarewave>= 0:
                    t2ch1.pulse_width_percent(100)
                else:
                    t2ch1.pulse_width_percent(0)
                if nextstate:
                    state =1
                    sinetime = utime.ticks_ms()
                    nextstate = False
                    print('Sine Wave Selected')

            else: 
                pass
        except KeyboardInterrupt:
            break
    ButtBreak = pyb.ExtInt(pinC13, mode = pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
    #Unassign interrupt callback button, so we can run script again later
