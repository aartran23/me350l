'''@file        Aaron_Says.py
@brief          Aaron says class file
@details        Implements a finite state machine to progress the game through multiple stages. Each round of 
                the game begins with a randomized sequence of blinks which 
                are randomly generated and somewhat resemble Morse Code.
                Players are to replicate the patterns played by the LED. 
                Code was deveoped in conjunction with Christopher Tan. Contributions to the file have been documented in the commit history of
                the shared repository, which is available in Figure 2.
                Source Code can be found at https://bitbucket.org/aartran23/me350l/src/master/Aaron_Says.py
                Video demonstration can be found at https://cpslo-my.sharepoint.com/:v:/g/personal/ctan13_calpoly_edu/ERN28FvdoO5EuVE33V_b54UBMvVmMwUgoTNt00AiCvFJ1g?e=LZtqN4
@image          html lab3std.jpg "Figure 1. State Transtion Diagram"
@image          html comm_hist.jpg "Figure 2. Screenshot of Commit History"
@author         Aaron Tran and Christopher Tan
@date           2/18/21
'''

import pyb
import utime
import random 
import micropython

class Simon:
    """@brief class file for playing Chris Says
    """
    
    # Static variables are defined outside of methods
    
    ##@brief initialization stage
    S0_INIT          = 0
    ##@brief generates a random pattern in this stage
    S1_FEED_PATT     = 1
    ##@brief plays generated pattern on LED in this stage
    S2_PLAY_PATT     = 2
    ##@brief player inputs pattern, which is automatically checked in this stage
    S3_CHECK_PATT    = 3
    ##@brief stage for completing a pattern of length 5
    S4_WIN           = 4
    ##@brief stage for incorrectly providing a pattern at any point
    S5_LOSE          = 5
    ##@brief stage for choosing whether to continue or stop playing
    S6_CONT_END      = 6
    ##@brief stage for providing win loss ratio and records
    S7_W_l_REC       = 7
    
   
    def __init__(self):
        """@brief The current state of the finite state machine
        """
        ##@brief state of Finite State Machine
        self.state = 0
               
        ##@brief    Maps pin C13 to variable
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        ##@brief    Maps pin A5 to variable
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

        ##@brief    Assigns interrupt callback to button
        pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=None)
        pyb.ExtInt(self.pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=None)
        ##@brief Assigns out interrupt callback
        self.ButtonInt = pyb.ExtInt(self.pinC13, mode = pyb.ExtInt.IRQ_RISING | pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.Button)
        ##@brief State of button, pressed/unpressed
        self.ButtState = "Unpressed"
        ##@brief Begin button press timer
        self.Start = 0
        ##@brief End button press timer
        self.End = 0
        ##@brief Duration of button press
        self.dur = utime.ticks_diff(self.End,self.Start)
        ##@brief Win record
        self.W = 0
        ##@brief Loss Record
        self.L = 0
        ##@brief Describes if first loop through state 0
        self.loop = 0
        ##@brief Describes if input signal has been received from button
        self.inputSignal = False
        ##@brief timestamp of user matching solution
        self.solve = False
        micropython.alloc_emergency_exception_buf(100)
        ##@brief global time of function light_play()
        self.lightglob = utime.ticks_ms()
        
    def run(self):
        """@brief main program
           @details The main program where it runs the game on the Nucleo board
                    This is where it simulates the random pattern on the LED
        """
        ##@brief global time of game
        self.globT = utime.ticks_ms()
        
        if self.state==self.S0_INIT:
            # run state 0 (init) code
            if self.loop == 0:
                print('Welcome! This game is called Chris Says.The LED on the ' 
                      +'Nucleo board will blink certain patterns. Copy those ' 
                      +'patterns using the user input button, B1. There will be '
                      +'two different possible inputs: 1 and 2, or short and long '
                      +'respectively. For none, wait for the input duration to finish. '
                      +'For short inputs, press for less than 500 ms. For long inputs, ' 
                      +'press for longer than 500 ms. Match 5 to win!' 
                      +'To begin playing, press the button.')
                ##@brief Array holding player input
                self.game_pat = []
                ##@brief Array holding randomly generated pattern
                self.pattern = []
                self.loop+=1
                self.solve = False
            if self.inputSignal:
                print('Starting Round')
                self.transitionTo(self.S1_FEED_PATT)   # Updating state for next iteration
                ##@brief start time of state 1
                self.S1T = utime.ticks_ms()
        elif self.state==self.S1_FEED_PATT:
            # run state 1 (in game) code
            ##@brief randomly generated addition to pattern
            self.curr_patt =random.randint(1,2)
            self.pattern.append(self.curr_patt)
            ##@brief describes how many times light_play has been called within state 2
            self.light_run = 0
            ##@brief forces light pattern to wait a little before playing
            self.delay = True
            ##@brief time in state two
            self.S2T = utime.ticks_ms()
            print('Playing pattern')
            self.transitionTo(self.S2_PLAY_PATT)
        elif self.state ==self.S2_PLAY_PATT:
            self.light_play(self.pattern[self.light_run])
        elif self.state ==self.S3_CHECK_PATT:
            if self.inputSignal:
                self.pattern_input()
            if self.solve:
                if utime.ticks_diff(self.globT, self.solveT)>= 500:
                    self.solve = False
                    self.transitionTo(self.S1_FEED_PATT)
        elif self.state==self.S4_WIN:
            self.W+=1
            print('Congratulations! You won!')
            self.transitionTo(self.S6_CONT_END)
            ##@brief local time of state 6
            self.S6_time = utime.ticks_ms()
            print('If you would like to continue, click the button within 5 seconds, otherwise do nothing for 5 seconds')
        elif self.state==self.S5_LOSE:
            # run state 3 (lost game) code
            self.L+=1
            print('Sorry, that was wrong')
            self.transitionTo(self.S6_CONT_END)    
            self.S6_time = utime.ticks_ms()
            print('If you would like to continue, click the button within 5 seconds, otherwise do nothing for 5 seconds')
        elif self.state==self.S6_CONT_END:
            if self.inputSignal:
                    self.transitionTo(self.S0_INIT)
                    self.inputSignal = False
                    self.loop = 0
            if utime.ticks_diff(self.globT, self.S6_time)>= 10000:
                    self.transitionTo(self.S7_W_l_REC)
                    ##@brief Describes if first loop through state 7
                    self.S7_loop = 0
            
        elif self.state==self.S7_W_l_REC:
            if self.S7_loop == 0:
                print('Wins:'+ str(self.W) + ' Losses: ' + str(self.L) + ' Win Rate:' + str(100*self.W/(self.L+self.W)) + '%')
                self.S7_loop+=1
            else:
                pass
        else:
            pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        """@brief Transition to a new state
           @param newState
        """
        self.state = newState
        self.inputSignal = False
   
    def light_play(self,pattern_pt):
        """@brief PWM of LED where it blinks
           @details It measures the time difference using utime which returns
                    an increasing millisecond counter with an arbitrary
                    reference point. When it is a given value the LED will
                    either turn on or turn off.
           @param pattern_pt
        """
        lightT = utime.ticks_ms()
        play_time = utime.ticks_diff(lightT, self.lightglob)
        if self.delay:
            if utime.ticks_diff(self.globT,self.S2T)>= 1000:
                self.lightglob = utime.ticks_ms()
                self.delay = False 
        elif pattern_pt == 1:
            if play_time<=100:
                self.pinA5.high()
            elif play_time>100 and play_time<600:
                self.pinA5.low()
            elif play_time>=600:
                self.light_run+= 1
                self.lightglob = utime.ticks_ms()
        elif pattern_pt == 2:
            if play_time<=600:
                self.pinA5.high()
            elif play_time>600 and play_time<1100:
                self.pinA5.low()
            elif play_time>=1100:
                self.light_run+= 1
                self.lightglob = utime.ticks_ms()
        if self.light_run == len(self.pattern):
            self.transitionTo(self.S3_CHECK_PATT)
            ##@brief index of generated pattern
            self.n = 0
            self.game_pat = []
            print('Input now')    
            
    def pattern_input(self):
        """@brief Intperprets button inputs and generates array to hold user input
        """
        self.dur = utime.ticks_diff(self.End,self.Start) 
        if self.dur<500:
            self.game_pat.append(1)
            if self.game_pat[self.n] != self.pattern[self.n]:
                self.transitionTo(self.S5_LOSE)
            self.n +=1
            print('Short Press Recorded')
        elif self.dur>= 500:
            self.game_pat.append(2)
            if self.game_pat[self.n] != self.pattern[self.n]:
                self.transitionTo(self.S5_LOSE)
            self.n +=1
            print('Long Press Recorded')
        if len(self.game_pat) == 5:
            self.transitionTo(self.S4_WIN) 
        if len(self.game_pat) == len(self.pattern):
            ##@brief time pattern is matched
            self.solveT = utime.ticks_ms()
            self.solve = True
        self.inputSignal = False
                      
    def Button(self, IRQ_src):
        """@brief Interrupt Callback Function
           @details Changes the state or allow the user to play the game 
                    by pressing the button after the nucleo board simulates
                    a random pattern.
           @param IRQ_src Interrupt Argument
        """
        if self.ButtState == 'Unpressed':
            self.ButtonStart()
        else:
            self.ButtonEnd()
            
    def ButtonStart(self):
        '''@brief Interrupt callback function; Captures initial button press
        '''
        self.pinA5.high()
        self.ButtState = 'Pressed' 
        if self.state== self.S3_CHECK_PATT:
            self.Start = utime.ticks_ms()
    
    def ButtonEnd(self):
        '''@brief Interrupt callback function; Captures end of button press
        '''
        self.pinA5.low()
        if self.state == self.S3_CHECK_PATT:   
            self.End = utime.ticks_ms()
        self.inputSignal = True
        self.ButtState = 'Unpressed'
