'''@file        lab4_1_DAQ.py
@brief          DAQ class file
@details        Implements a finite state machine to simulate data sampling
                and print it, intended to be ran on a Nucleo
                Source Code can be found at https://bitbucket.org/aartran23/me350l/src/master/Aaron_Says.py
@image          html STD.jpg "State Transtion Diagram"
@author         Aaron Tran
@date           2/25/21
'''
from pyb import UART
from math import exp, sin, pi
from array import array
import utime

class DAQ:
    ##@brief State 0, initialization
    S0_INIT            = 0
    ##@brief State 1, data collection
    S1_COLLECT         = 1
    ##@brief State 2, print data
    S2_PRINT           = 2
    
    
    def __init__(self):
        ##@brief csv index
        self.n = 0
        ##@brief Current State
        self.state = 0
        ##@brief handle for UART port 
        self.myuart = UART(2)
        ##@brief time data
        self.times  = array('f', list(time/100 for time in range(0,3001)))
        ##@brief value data
        self.values = array('f', 3001*[0])

    def run(self):
        '''@brief Samples and prints data
        '''
        ##@brief global time
        self.globT = utime.ticks_ms()
        if self.state == self.S0_INIT:
            if self.myuart.any() != 0:
                ##@brief ASCII encoded data input
                self.val = self.myuart.readchar()
                if self.val == 103:
                    ##@brief state 1 beginning time
                    self.S1T = utime.ticks_ms()
                    ##@brief 10ms duration start
                    self.thisTime = utime.ticks_ms()
                    self.transitionTo(self.S1_COLLECT)
        elif self.state == self.S1_COLLECT:
            if utime.ticks_diff(self.globT, self.thisTime)>=10:
                ##@brief Beginning of 10 ms duration between sampling
                self.thisTime = utime.ticks_ms()
                self.values[self.n] = exp(-self.times[self.n]/10)*sin(2*pi/3*self.times[self.n])
                self.n += 1
            if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                if self.val == 115:
                    self.transitionTo(self.S2_PRINT)
            elif utime.ticks_diff(self.globT, self.S1T)>=30000:
                self.transitionTo(self.S2_PRINT)
            pass
        elif self.state == self.S2_PRINT:
            for n in range(len(self.times)):
                self.myuart.write('{:}, {:}'.format(self.times[n], self.values[n]))
            self.transitionTo(self.S0_INIT)
        else:
            print('Error in FSM occurred')
    
    
    def transitionTo(self, newState):
        """@brief Transition to a new state
           @param newState
        """
        self.state = newState
        