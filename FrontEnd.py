'''@file        FrontEnd.py
@brief          interprets Nucleo data and generates .CSV file
@details        This is the file intended to be ran on the computer. It receives serial data from main.py, specifically from the UI_task.py. This code implements a finite state machine, whose state transition diagram can be found in Figure 1.
                Data collection for the PC can be conducted using this script. This script will generate a .CSV file containing relevant data, as well as plots of the data. 
                The .CSV file will be named lab4data.csv, with some numerical suffix representing the version of data created. Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/FrontEnd.py
@image          html frontSTD.jpg "Figure 1. State Transition Diagram of FrontEnd.py"
@author         Aaron Tran
@date           3/17/21
'''
import serial
import keyboard
from matplotlib import pyplot
import shares
import csv
from array import array


def kb_cb(key):
    """@brief Callback function which is called when a key has been pressed.
    @param key interrupt callback source
    """
    global last_key
    ##@brief last key pressed
    last_key = key.name
    if last_key != 'n':
        ser.write(str(last_key).encode('ascii'))
        
def make_csv(file,x1,y1,x2,y2):
    '''@brief writes data to csv file
    @param file existing file to be used to write the CSV data into
    @param x1 x values of first data set
    @param y1 y values of first data set
    @param x2 x values of second data set
    @param y2 yvalues of second data set
    '''
    n = 0
    with open(file, mode = 'w',newline='') as data:
        data = csv.writer(data, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data.writerow(['Time [s]', 'Encoder Position [deg]','Time [s]', 'Encoder Speed [RPM]'])
        for point in x1:
            data.writerow([str(x1[n]), str(y1[n]),str(x2[n]), str(y2[n])])
            n+=1
    pass


def curtail(array1, array2):
    '''@brief cuts off extraneous data due to array preallocation
    @param array1 time array
    @param array2 corresponding data to time array
    '''
    ind = 0
    flag = []
    for n in array1:
        if n == 0 and ind != 0:
            flag.append(ind)
        ind+=1
    for n in flag[::-1]:
        array1.pop(n)
        array2.pop(n)
        
        
if __name__ == "__main__":
    ##@brief stage of script
    state = 0
    keyboard.on_release_key("S", callback=kb_cb)
    keyboard.on_release_key("G", callback=kb_cb)
    keyboard.on_release_key("Z", callback=kb_cb)
    keyboard.on_release_key("P", callback=kb_cb)
    keyboard.on_release_key("D", callback=kb_cb)
    keyboard.on_release_key("A", callback=kb_cb)
    keyboard.on_release_key("K", callback=kb_cb)

    ##@brief handle for serial port
    ser = serial.Serial(port='COM5', baudrate=115273, timeout=1)
    ##@brief last key pressed    
    last_key = None
    ##@brief describes runs through stages
    first_run = True
    ##@brief describes how many .csv files have been generated
    ver = 0
    ##@brief reference time values
    time_ref  = array('f', 751*[0])
    ##@brief reference angular speed values
    omega_ref  = array('f', 751*[0])
    ##@brief reference position values
    theta_ref  = array('f', 751*[0])
    with open('reference.csv', 'r') as csv_file:
        ##@brief list index value
        i=0
        try:
            for line in csv_file:
                ##@brief string containing reference values
                line_Str=csv_file.readline()
                line_Str = line_Str.strip()
                # line_Str=line_Str.rstrip('\n')
                # line_Str=line_Str.rstrip('\r')
                line_Str=line_Str.rsplit(',')
                time_ref[i] = float(line_Str[0])
                omega_ref[i] = float(line_Str[1])
                theta_ref[i] = float(line_Str[2])
                i+=1
        except:
            pass                    
    while True:
        try:
            ##@brief string received from Nucleo
            myLineString = ser.readline().decode()
            ##@brief myLineString stripped of extraneous string data like \n and \r
            myStrippedString = myLineString.strip()
            ##@brief list containing data in each CSV
            mySplitStrings = myStrippedString.split(',')
            if mySplitStrings[0] == 'UI':
                print(mySplitStrings[1])
                last_key = None
            if state ==0:
                ##@brief time data corresponding to position data
                times_pos = []
                ##@brief time data corresponding to speed data
                times_speed = []
                ##@brief Motor 1 position data
                enc1_pos  = []
                ##@brief Motor 1 speed data
                enc1_speed = []
                ##@brief Motor 1 error
                enc1_err = []
                ##@brief Motor 2 position data
                enc2_pos  = []
                ##@brief Motor 2 speed data
                enc2_speed = []
                ##@brief Motor 2 error data
                enc2_err = []
                if first_run:
                    first_run = False
                    print(' Press g to begin taking data for 15 seconds\n Press s to stop taking data prematurely\n Press z to zero encoder\n Press p to print encoder position\n Press d to print last delta\n Press k to turn off motors\n Press a to turn motors on')     
                if last_key == 'g':
                    last_key = None
                    state = 1
                    first_run = True
            elif state == 1:
                if first_run:
                    first_run = False
                    print('Press s to prematurely stop data collection')
                try:
                    if mySplitStrings[0] == 'Done':
                        state = 2
                    elif mySplitStrings[1] == 0 and len(times_pos) > 10:
                        state = 2
                    elif mySplitStrings[0] == 'enc1_pos':  
                        times_pos.append(float(mySplitStrings[1]))
                        enc1_pos.append(float(mySplitStrings[2]))
                    elif mySplitStrings[0] == 'enc1_speed': 
                        times_speed.append(float(mySplitStrings[1]))
                        enc1_speed.append(float(mySplitStrings[2]))  
                    elif mySplitStrings[0] == 'enc1_err':
                        enc1_err.append(float(mySplitStrings[1]))                        
                    elif mySplitStrings[0] == 'enc2_pos':  
                        times_pos.append(float(mySplitStrings[1]))
                        enc2_pos.append(float(mySplitStrings[2]))
                    elif mySplitStrings[0] == 'enc2_speed': 
                        times_speed.append(float(mySplitStrings[1]))
                        enc2_speed.append(float(mySplitStrings[2]))     
                    elif mySplitStrings[0] == 'enc2_err':
                        enc2_err.append(float(mySplitStrings[1]))
                except:
                    pass
            elif state == 2:
                #Post Processing
                print('you made it state two')
                if shares.motor == 1:
                    curtail(times_speed, enc1_speed)
                    curtail(times_pos, enc1_pos)
                elif shares.motor == 2:
                    curtail(times_speed, enc2_speed)
                    curtail(times_pos, enc2_pos)

                try: 
                     ##@brief .CSV filename 
                     filename = 'lab4data('+str(ver)+').csv'
                     ##@brief generates .CSV file                      
                     f = open(filename, "x")
                except:
                    ver+=1
                    if shares.motor == 1:
                        make_csv(filename,times_pos, enc1_pos, times_pos, enc1_speed)                        
                    if shares.motor == 2:
                        make_csv(filename,times_pos, enc2_pos, times_pos, enc2_speed)
                if shares.motor == 1:
                    #Motor 1
                    pyplot.figure()
                    pyplot.plot(times_speed, enc1_speed, 'k:', label = 'Encoder')
                    pyplot.plot(time_ref, omega_ref,'k', label = 'Reference')
                    pyplot.title('Motor 1')
                    pyplot.legend(loc='lower right', shadow=True, fontsize='medium')
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Angular Speed [RPM]')
                    pyplot.show()              
                    
                    pyplot.figure()
                    pyplot.plot(times_pos, enc1_pos, 'k:', label = 'Encoder')
                    pyplot.plot(time_ref, theta_ref,'k--', label = 'Reference')
                    pyplot.title('Motor 1')
                    pyplot.legend(loc='lower right', shadow=True, fontsize='medium')                    
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Position [degrees]')
                    pyplot.show()              
                    print('switching states')
                    first_run = True
                    state = 0
                    ##Error in motor
                    J_mot = sum(enc1_err)/len(enc1_err)
                    print("The error value is" + str(J_mot))
                elif shares.motor == 2:
                    #Motor 2
                    pyplot.figure()
                    pyplot.plot(times_speed, enc2_speed, 'k:', label = 'Encoder')
                    pyplot.plot(time_ref, omega_ref,'k', label = 'Reference')
                    pyplot.legend(loc='lower right', shadow=True, fontsize='medium')
                    pyplot.title('Motor 2')
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Angular Speed [RPM]')
                    pyplot.show()              
                    
                    pyplot.figure()
                    pyplot.plot(times_pos, enc2_pos, 'k:', label = 'Encoder')
                    pyplot.plot(time_ref, theta_ref,'k', label = 'Reference')
                    pyplot.legend()
                    pyplot.title('Motor 2')
                    pyplot.legend(loc='lower right', shadow=True, fontsize='medium')
                    pyplot.xlabel('Time')
                    pyplot.ylabel('Position [degrees]')
                    pyplot.show()              
                    print('switching states')
                    first_run = True
                    state = 0
                    J_mot = sum(enc2_err)/len(enc2_err)
                
                    print("The error value is " + str(J_mot))
            else:
                print('Something happened')
                print('state:' + str(state))
                ser.close()
                keyboard.unhook_all()
                break

            
        except KeyboardInterrupt:
            ser.close()
            keyboard.unhook_all()
            print('You exited with Ctrl + C')
            break
    
    
