## \page Home
#  \tableofcontents
#
#  \section sec Home Page
#  This is Aaron Tran's (my) ME305 portfolio. It contains all the code developed throughout the Winter 2021 quarter. All code was deisgned to be implemented on a Nucleo STM32. The Nucleo board that I used for
#  Labs 2, 3, and a little bit of Lab 4 before transitioning to the hardware provided by my professor, Charlie Refvem, is shown in Figure 1.
#  ME305 is an Introduction to Mechatronics class. As such, the work provided in this portfolio shows implementation of working with code and interfacing with hardware to eventually create a mechanical
#  action. This is all culminated into the ME305 Term Project. All labs before this act to familiarize me with using the tools necessary like MicroPython and the Nucleo. 
## \image html nucleo.jpg "Figure 1. Nucleo STM32L476RG Development Board that I used for labs related to hardware in order to familiarize myself with MicroPython"
#
#  \subsection FSM HW 0x02: Implementing FSMs in Python
#  In the interest of preparing ourselves for machines, we learned how to create a Finite State Machine (FSM). This allows us to discretize our codes functions, which helps organize code and 
#  improve code functionality. To first practice this concept, I completed a homework assignment that simulates two floor elevator movement. The code for this is available in FSM_elevator.py.
#
#  \subsection Lab1 Lab 0x01: Fibonacci
#  For the first lab of ME305, we were interested in familiarizing ourselves with coding in python and using the development environment Spyder. To do so,
#  I wrote a fibonacci sequence calculator that will return the fibonacci number at a given index with base 0. For example if one were to input an index of 3, the code would
#  output the fourth number in the fibonacci sequence, 3. Refer to fibonacci.py for the code. 
#
#  \subsection Lab2 Lab 0x02: Getting Started with Hardware
#  Having gotten familiar with writing code in python, we are now interested in interfacing with hardware. To do this, I used MicroPython programming
#  and the Nucleo development board. The task of this lab was to generate multiple different waveforms on an LED on the Nucleo. These waveforms can be cycled through
#  by using the blue USER button on the Nucleo. The waveforms include a sinewave, sawtooth wave, and squarewave. Refer to Lab2.1.py for the code.
#
#  \subsection Lab3 Lab 0x03: Simon Says
#  To further familiarize ourselves with the hardware, we developed a game using the Nucleo development board. I partnered up with Christopher Tan to create 
#  Chris Says. This game blinks an LED for random periods of time. The player is copies the pattern, by pressing down on the blue USER button in a pattern to copy the one given by the LED.
#  Successfully copying a pattern of length 5 results in a win, where the player is then allowed to choose between continuing on to play more, or ending the game and seeing their W/L score.
#  Refer to Aaron_Says.py for the class file that was used to write the methods for the game and lab3.py which implements the class.
#
#  \subsection Lab4 Lab 0xFF: Term Project
#  To finish off the Winter 2021 quarter, we developed a motor controller, motor drivers, a DAQ, and learned to use serial communication. For more information on this refer to \ref intro "ME305 Term Project".