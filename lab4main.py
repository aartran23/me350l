'''@file        lab4main.py
@brief          interprets Nucleo data and generates .CSV file
@details        main file intended to be ran on computer in conjunction with DAQ class 
                file on Nucleo, interprets Nucleo output and generates .CSV file
@author         Aaron Tran and Christopher Tan
@date           2/25/21
'''
import serial
import keyboard
from matplotlib import pyplot

from array import array
import csv


def kb_cb(key):
    """@brief Callback function which is called when a key has been pressed.
    @param key interrupt callback source
    """
    global last_key
    ##@brief last key pressed
    last_key = key.name
    ser.write(str(last_key).encode('ascii'))

def make_csv(file,x,y):
    '''@brief writes data to csv file
    '''
    n = 0
    with open(file, mode = 'w',newline='') as data:
        data = csv.writer(data, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for point in x:
            data.writerow([str(x[n]), str(y[n])])
            n+=1
    pass



if __name__ == "__main__":
    ##@brief stage of script
    state = 0
    keyboard.on_release_key("S", callback=kb_cb)
    keyboard.on_release_key("G", callback=kb_cb)
    ##@brief handle for serial port
    ser = serial.Serial(port='COM3', baudrate=115273, timeout=1)
    last_key = None
    ##@brief describes runs through stages
    first_run = True
    while True:
        try:
            if state ==0:
                ##@brief array containing time data
                times  = array('f', list(time/100 for time in range(0,3001)))
                ##@brief array containing value data
                values = array('f', 3001*[0])
                n=0
                if first_run:
                    first_run = False
                    print('Press g to begin taking data')
                if last_key == 'g':
                    last_key = None
                    state = 1
                    first_run = True
            elif state == 1:
                if first_run:
                    first_run = False
                    print('Press s to prematurely stop data collection')
                ##@brief string retrieved from Nucleo
                myLineString = ser.readline().decode('ascii')
                ##@brief string received from Nucleo without line endings
                myStrippedString = myLineString.strip()
                ##@brief list object containing time and value data
                mySplitStrings = myStrippedString.split(',')
                if mySplitStrings[0] != '' and mySplitStrings[0] != 'g' and mySplitStrings[0] != 's' and mySplitStrings[0] != '>>>': 
                    times[n] = float(mySplitStrings[0])
                    values[n] = float(mySplitStrings[1])
                    n+=1
                if n >= 3001:
                    break
            else: 
                break
            
        except KeyboardInterrupt:
            ser.close()
            keyboard.unhook_all()
            break
    
    
    try: 
        ##@brief generates .CSV file 
        f = open('lab4data.csv', "x")
    except:
        pass
    make_csv('lab4data.csv',times, values)  
    pyplot.figure()
    pyplot.plot(times, values)
    pyplot.xlabel('Time')
    pyplot.ylabel('Data')
    ser.close()
    keyboard.unhook_all()