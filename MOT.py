
'''@file        MOT.py
@brief          Use to send PWM to motor and control motor motion. This class and its methods are intended to be used and called by CTRL_task.py..
@details        This class contains methods for turning on the motor, turning off the motor, and setting the PWM of the motor. 
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/MOT.py
@author         Aaron Tran
@date           3/17/21
'''

import pyb
class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        '''@brief Creates a motor driver by initializing GPIO pins
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timer A pyb.Timer object to use for PWM generation on
        IN1_pin and IN2_pin. '''
        ##@brief pin responsible for turning motor on and off
        self.EN_pin = nSLEEP_pin
        ##@brief A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin
        self.timer = timer
        if IN1_pin == pyb.Pin.cpu.B4 or IN1_pin == pyb.Pin.cpu.B5:
            IN1CH = 1
            IN2CH = 2
        elif IN1_pin == pyb.Pin.cpu.B0 or IN1_pin == pyb.Pin.cpu.B1:
            IN1CH = 3
            IN2CH = 4
        ##@brief timer channel used for one direction of motion
        self.IN1 = self.timer.channel(IN1CH, mode=pyb.Timer.PWM, pin=IN1_pin)
        ##@brief timer channel used for opposite direction of motion from self.IN1       
        self.IN2 = self.timer.channel(IN2CH, mode=pyb.Timer.PWM, pin=IN2_pin)
        print ('Creating a motor driver')
    
    def enable (self):
        ''' @details turns motor on
        '''
        EN = pyb.Pin(self.EN_pin, mode=pyb.Pin.OUT_PP, value=1)
        print ('Enabling Motor')

    def disable (self):
        '''@details turns motor off
        '''
        EN = pyb.Pin(self.EN_pin, mode=pyb.Pin.OUT_PP, value=0)
        print ('Disabling Motor')

    def set_duty (self, duty):
        '''@brief sets PWM of motor 
        @details This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        if duty >= 0:
            PWM_1 = duty
            PWM_2 = 0
        elif duty < 0:
            PWM_1 = 0
            PWM_2 = abs(duty)
        self.IN1.pulse_width_percent(PWM_1)
        self.IN2.pulse_width_percent(PWM_2)

# if __name__ == '__main__':
#      # Adjust the following code to write a test program for your motor class. Any
#      # code within the if __name__ == '__main__' block will only run when the
#      # script is executed as a standalone program. If the script is imported as
#      # a module the code block will not run.

#      # Create the pin objects used for interfacing with the motor driver
#      pin_nSLEEP = pyb.Pin.cpu.A15
#      pin_IN1 = pyb.Pin.cpu.B4
#      pin_IN2 = pyb.Pin.cpu.B5
     
#      pin_nSLEEP2 = pyb.Pin.cpu.A15
#      pin_IN12 = pyb.Pin.cpu.B0
#      pin_IN22 = pyb.Pin.cpu.B1
     
#      # Create the timer object used for PWM generation
#      tim = pyb.Timer(3, freq=20000)
     
#      # Create a motor object passing in the pins and timer
#      moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
#      moe2 = MotorDriver(pin_nSLEEP2, pin_IN12, pin_IN22, tim)
#      # Enable the motor driver
#      moe.enable()
#      moe2.enable()
#      # Set the duty cyc
#      moe.set_duty(100)
#      moe2.set_duty(100)
#      delay(5000)
#      moe.set_duty(50)
#      moe.set_duty(50)
#      delay(5000)
#      moe.set_duty(-50)
#      moe.set_duty(-50)
#      delay(5000)     
#      moe.set_duty(-100)
#      moe.set_duty(-100)
#      delay(5000)
#      moe.disable()
#      moe2.disable()
     