'''@file    FSM_elevator.py
@brief      Simulates an elevator that moves between two floors
@details    Implements a finite state machines shown below, to simulate
            the behavior of an elevator 
            Source https://bitbucket.org/aartran23/me305l/src/master/FSM_elevator.py
@image      html elevator.png "Figure 1. Finite Stat Machine:Elevator, State Transition Diagram"
@author     Aaron Tran
@date       January 25, 2021                
'''

import time
import random
# Function definitions go here
def motor(cmd):
    '''@brief Commands elevator motor to control elevator motion
       @param cmd  The command to give the motor
    '''
    if cmd == 0:
        print('Elevator is Stationary')
    elif cmd == 1:
        print('Elevator is Moving Up')
    elif cmd == 2:
        print('Elevator is Moving Down')
def button_1(clear):
    '''@brief Simulates Floor 1 button being pressed
       @param clear Allows for clearing or simulating user input
    '''
    if clear == 'clear':
        return False
    else:    
        return random.choice([True,False]) # randomly returns T or F
def button_2(clear):
    '''@brief Simulates Floor 2 button being pressed
       @param clear Allows for clearing or simulating user input
    '''
    if clear == 'clear':
        return False
    else:    
        return random.choice([True,False]) # randomly returns T or F
def floor_1():
    '''@brief Tells elevator if it is on floor 1
    '''
    return random.choice([True,False]) # randomly returns T or F
def floor_2():
    '''@brief Tells elevator if it is on floor 2
    '''
    return random.choice([True,False]) # randomly returns T or F


# Main program / test program begin 
#   This code only runs if the script is executed as a main by pressing play 
#   but does not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    print('Powering Up Elevator')
    ## State that Finite State Machine is in
    state = 0 # Initial state is the init state
    while True:
        try:
            # main program code goes here
            if state ==0:
                pass
                print('S0')
                motor(2) # Command motor to move down
                if floor_1():
                    motor(0)
                    button_1('clear')
                    state = 1   # Updating state for next iteration
                # run state 0 (Moving Down) code
            elif state ==1:
                print('S1')
                # run state 1 (Stopped on Floor 1) code
                # If we are at the left, stop the motor and transition to S2
                if button_2('waiting for input'):
                    motor(1)
                    state= 2
                elif button_1('waiting for input'):
                    button_1('clear')
            elif state ==2:
                print('S2')
                # run state 2 (Moving Up) code    
                if floor_2():
                    motor(0)
                    state =3
                    button_2('clear')
            elif state ==3:
                print('S3')
                # run state 3 (Stopped on Floor 2) code    
                if button_1('waiting for input'):
                    motor(2)
                    state = 0
                elif button_2('waiting for input'):
                    button_2('clear')
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
                
            # Slow down execution of FSM so we can see output in console
            time.sleep(1)
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard breaking
            # while(True) loop when desired
            break
        
    # Program de-initialization goes here
    print('Elevator has been deactivated')