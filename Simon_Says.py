
import pyb
import utime

class Simon:
    
    # Static variables are defined outside of methods
    
    ## State 1 constant
    S0_INIT          = 0
    S1_IN_GAME       = 1
    S2_WIN           = 2
    S3_LOSE          = 3
    S4_CONT_END      = 4
    S5_W_l_REC       = 5
    
   
    def __init__(self, period, DBG_flag=True):
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
          
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ##@brief    Maps pin C13 to variable
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        ##@brief    Maps pin A5 to variable
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        ##@brief    Sets PWM of LED
        self.tim2 = pyb.Timer(2, freq = 20000)
        ##@brief    Gives pin A5 PWM value to run at
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        ##@brief    Assigns interrupt callback to button
        self.ButtRise = pyb.ExtInt(self.pinC13, mode = pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=self.ButtonStart)
        self.ButtFall = pyb.ExtInt(self.pinC13, mode = pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.ButtonEnd)
        self.game_pat = []
        self.W = 0
        self.L = 0
    
    def run(self):
        thisTime = utime.ticks_us()
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            self.runs += 1
            # if self.DBG_flag:
            #     
            # main program code goes here
            if self.state==self.S0_INIT:
                # run state 0 (init) code
                self.transitionTo(self.S1_IN_GAME)   # Updating state for next iteration
                
            elif self.state==self.S1_IN_GAME:
                # run state 1 (moving left) code
                # If we are at the left, stop the motor and transition to S2
                #if self.L_sensor():
                self.transitionTo(self.S2_STOPPED_LEFT)
                if not self.win() & self.pattern_match():
                    self.n+=1
                    self.pattern(self.n)
                if self.win():
                    self.W+=1
                    self.transitionTo(self.S3_MOVING_RIGHT)
                if self.lose():
                    self.L+=1
                    self.transitionTo(self.S3_MOVING_RIGHT)
            elif self.state==self.S2_WIN:
                # run state 2 (stopped at left) code
                # if go button is pressed, start motor and transition to S3
                self.transitionTo(self.S4_CONT_END)
            elif self.state==self.S3_LOSE:
                # run state 3 (moving right) code
                # If we are at the right, stop the motor and transition to S4
                #if self.R_sensor():
                    self.transitionTo(self.S4_CONT_END)
                
            elif self.state==self.S4_CONT_END:
                # run state 4 (stopped at right) code
                self.transitionTo(self.S1_MOVING_LEFT)   # Updating state for next iteration
            
            elif self.state==self.S5_W_l_REC:
                self.transitionTo(self.S0_INIT)   # Updating state for next iteration
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
    
    def pattern_match(self):
        #still need to implement way to only define self.dur once buttons are 
        #and not use old button values, also need to figure out how to store a 0
        #if its been more than a second and no button has been pressed 
        self.dur = utime.ticksdiff(self.ButtonEnd(),self.ButtonStart())
        if self.dur<200:
            self.game_pat.append(1)
        elif self.dur>= 200:
            self.game_pat.append(2)
        elif self.game_pat == self.pattern:
            return True
    def pattern(self,n):
        self.pattern_set = [0, 1, 0, 1, 2, 2, 1 ,0] 
        return(self.pattern_set[0:n])
    
    def ButtonStart(self,IRQ_src):
        '''@brief Interrupt callback function; Captures initial button press
        @param IRQ_src Interrupt argument
        '''
        return utime.ticks_ms()
    def ButtonEnd(self, IRQ_src):
        '''@brief Interrupt callback function; Captures end of button press
        @param IRQ_src Interrupt argument
        '''
        return utime.ticks_ms()
