# -*- coding: utf-8 -*-
'''@file        UI_task.py
@brief          Communicates with computer. This is intended to be called by main.py
@details        Uses serial communication to send data from Nucleo to PC. Responsible for sending encoder position, speed, and error to the computer.
                Data is sent using UART. This code implements a finite state machine
                to initialize, collect date, and send it to the computer. The corresponding state transition diagram can be found in Figure 1.
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/UI_task.py
@image          html UIstd.jpg "Figure 1. State Transition Diagram"
@author         Aaron Tran
@date           3/17/21
'''
import pyb
from pyb import UART
from array import array
import utime
import shares

class UI_task:
    '''This class implements serial communication and user interface methods on the ME405 board
    '''
    ##@brief State 0, initialization
    S0_INIT            = 0
    ##@brief State 1, data collection
    S1_COLLECT         = 1
    ##@brief State 2, print data
    S2_PRINT           = 2
    
    
    def __init__(self):
        '''@brief initializes UI_task
        '''
        ##@brief csv index
        self.n = 0
        ##@brief Current State
        self.state = 0
        ##@brief handle for UART port 
        self.myuart = UART(2)
        ##@brief ASCII encoded data input        
        self.val = None
        ##@brief periodic timestamp 
        self.timestamp = utime.ticks_ms()
             
        print('UI Task Initialized')
    def run(self):
        '''@brief Samples and prints data
        '''
        ##@brief global time
        self.globT = utime.ticks_ms()
        if utime.ticks_diff(self.globT,self.timestamp)>= 500:    
            self.timestamp = utime.ticks_ms()
            print(self.state)
        if self.myuart.any() != 0:
                self.val = self.myuart.readchar()
                print('You pressed ASCII' + str(self.val))
                if self.val == 122:
                    print('You have zeroed the encoder')
                    shares.zero_enc = True
                    self.myuart.write('{:}, {:}\r\n'.format('UI', 'You have zeroed the encoders'))
                    self.val = None
                elif self.val == 112:
                    if shares.motor == 1:
                        self.myuart.write('{:}, {:}\r\n'.format('UI', 'The position of encoder 1 is: '+ str(shares.enc1_pos)))
                        print('The position of the encoder 1 is: '+ str(shares.enc1_pos))
                    if shares.motor == 2:
                        self.myuart.write('{:}, {:}\r\n'.format('UI', 'The position of encoder 2 is: '+ str(shares.enc2_pos)))
                        print('The position of the encoder 2 is: '+ str(shares.enc2_pos))
                    self.val = None
                elif self.val == 100: 
                    shares.get_delta = True
                    self.val = None
                elif self.val == 107:
                    shares.kill = True
                elif self.val == 97:
                    shares.revive = True
        if shares.UI_delta:
            if shares.motor == 1:
                self.myuart.write('{:}, {:}\r\n'.format('UI', 'The last delta of encoder 1 was: '+ str(shares.enc1_delta)))
                print('The the last delta of encoder 1 was: '+ str(shares.enc1_delta))
            if shares.motor == 2:
                self.myuart.write('{:}, {:}\r\n'.format('UI', 'The last delta of encoder 2 was: '+ str(shares.enc2_delta)))            
                print('The the last delta of encoder 2 was: '+ str(shares.enc2_delta))
            shares.UI_delta = False            
            pass
        if self.state == self.S0_INIT:
                if self.val == 103:
                    shares.revive = True
                    ##@brief state 1 beginning time
                    self.S1T = utime.ticks_ms()
                    ##@brief time at the start of each 20ms period in state 1
                    self.thisTime = utime.ticks_ms()
                    print('you\'re in state 1')
                    self.n = 0
                    ##@brief time data of encoder 1
                    self.times1 = array('f', 751*[0])
                    ##@brief speed data of encoder 1 [RPM]
                    self.speed1 = array('f', 751*[0])       
                    ##@brief position data of encoder 1 [deg]
                    self.pos1 = array('f', 751*[0])
                    ##@brief error data of motor 1
                    self.enc1_err =  array('f', 751*[0])
                    ##@brief time data of encoder 2
                    self.times2  = array('f', 751*[0])
                    ##@brief speed data of encoder 2 [RPM]
                    self.speed2 = array('f', 751*[0])     
                    ##@brief position data of encoder 2 [deg]
                    self.pos2 = array('f', 751*[0])   
                    ##@brief error data of motor 2
                    self.enc2_err =  array('f', 751*[0])                    
                    self.transitionTo(self.S1_COLLECT)
                    self.val = None
        elif self.state == self.S1_COLLECT:
            if utime.ticks_diff(self.globT, self.thisTime)>=20:
                print('update')
                self.thisTime = utime.ticks_ms()
                
                self.times1[self.n] = utime.ticks_diff(self.globT,self.S1T)/1000
                self.speed1[self.n] = shares.enc1_speed
                self.pos1[self.n] = shares.enc1_pos 
                self.enc1_err[self.n] = shares.enc1_err
                
                self.times2[self.n] = utime.ticks_diff(self.globT,self.S1T)/1000
                self.speed2[self.n] = shares.enc2_speed
                self.pos2[self.n] = shares.enc2_pos                
                self.enc2_err[self.n] = shares.enc2_err
                
                self.n += 1
            if self.val == 115:
                print('you\'re in state 2')
                self.times1 = self.times2[0:(self.n-1)]
                self.speed1 = self.speed2[0:(self.n-1)]  
                self.pos1 = self.pos2[0:(self.n-1)]
                
                self.times2 = self.times2[0:(self.n-1)]
                self.speed2 = self.speed2[0:(self.n-1)]  
                self.pos2 = self.pos2[0:(self.n-1)]
                
                self.transitionTo(self.S2_PRINT)
                self.val = None
            elif utime.ticks_diff(self.globT, self.S1T)>=15000:
                print('you\'re in state 2')     
                self.transitionTo(self.S2_PRINT)
            pass
        elif self.state == self.S2_PRINT:
            for n in range(len(self.times1)):
                    if shares.motor == 1:
                        self.myuart.write('{:}, {:}, {:}\r\n'.format('enc1_speed', self.times1[n], self.speed1[n]))
                        self.myuart.write('{:}, {:}, {:}\r\n'.format('enc1_pos', self.times1[n], self.pos1[n]))
                        self.myuart.write('{:}, {:}\r\n'.format('enc1_err', self.enc1_err[n]))                        
                        print('{:}, {:}, {:}'.format('enc1_speed', self.times1[n], self.speed1[n]))
                        print('{:}, {:}, {:}'.format('enc1_pos', self.times1[n], self.pos1[n]))
                    if shares.motor == 2:
                       self.myuart.write('{:}, {:}, {:}\r\n'.format('enc2_speed', self.times2[n], self.speed2[n]))
                       self.myuart.write('{:}, {:}, {:}\r\n'.format('enc2_pos', self.times2[n], self.pos2[n]))
                       self.myuart.write('{:}, {:}\r\n'.format('enc2_err', self.enc2_err[n]))                                              
                       print('{:}, {:}, {:}'.format('enc2_speed', self.times2[n], self.speed2[n]))
                       print('{:}, {:}, {:}'.format('enc2_pos', self.times2[n], self.pos2[n]))

            print('Done')
            self.myuart.write('Done,Done\r\n')
            self.transitionTo(self.S0_INIT)        
        else:
            print('Error in FSM occurred')

    
    def transitionTo(self, newState):
        """@brief Transition to a new state
           @param newState state to transition to
        """
        self.state = newState
        
 
                
            