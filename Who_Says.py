import pyb
import utime
import random 

class Simon:
    
    # Static variables are defined outside of methods
    
    ## State 1 constant
    S0_INIT          = 0
    S1_FEED_PATT     = 1
    S2_PLAY_PATT     = 2
    S3_CHECK_PATT    = 3
    S4_WIN           = 4
    S5_LOSE          = 5
    S6_CONT_END      = 6
    S7_W_l_REC       = 7
    
   
    def __init__(self, period, DBG_flag=True):
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
         
        ## Timestamp for last iteration of the task in microseconds
        #  could eliminate for concision if desired
        self.lastTime = utime.ticks_us()
        
        ## The input duration time for gameplay
        self.period = period
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ##@brief    Maps pin C13 to variable
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        ##@brief    Maps pin A5 to variable
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5)
        ##@brief    Sets PWM of LED
        self.tim2 = pyb.Timer(2, freq = 20000)
        ##@brief    Gives pin A5 PWM value to run at
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        ##@brief    Assigns interrupt callback to button
        self.ButtRise = pyb.ExtInt(self.pinC13, mode = pyb.ExtInt.IRQ_RISING, pull=pyb.Pin.PULL_NONE, callback=self.ButtonStart)
        self.ButtFall = pyb.ExtInt(self.pinC13, mode = pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.ButtonEnd)
        self.game_pat = []
        self.pattern = []
        self.W = 0
        self.L = 0
    
    def run(self):
        self.globT = utime.ticks_us()
        
        if self.state==self.S0_INIT:
            # run state 0 (init) code
            self.transitionTo(self.S1_IN_GAME)   # Updating state for next iteration
            
        elif self.state==self.S1_FEED_PATT:
            # run state 1 (in game) code
            # If we are at the left, stop the motor and transition to S2
            self.curr_patt =random.randint(0,2)
            self.pattern.append(self.curr_patt)
            self.transitionTo(self.S7_PLAY_PATT)
            self.light_glob = utime.ticks_us()
            self.light_run = 0
        elif self.state ==self.S2_PLAY_PATT:
            self.light_play(self.pattern[self.light_run])
            if self.light_done:
                self.transitionTo(self.S3_CHECK_PATT)
        elif self.state ==self.S3_CHECK_PATT:
            if len(self.pattern)!=7 & self.pattern_match():
                    self.n+=1
                    self.pattern(self.n)
            if len(self.pattern)==7 & self.pattern_match():
                    self.W+=1
                    self.transitionTo(self.S4_WIN)
            if not self.pattern_match():
                    self.L+=1
                    self.transitionTo(self.S5_LOSE)
        elif self.state==self.S4_WIN:
            # run state 2 (won game) code
            # if go button is pressed, start motor and transition to S3
            self.transitionTo(self.S6_CONT_END)
        elif self.state==self.S5_LOSE:
            # run state 3 (lost game) code
            # If we are at the right, stop the motor and transition to S4
            #if self.R_sensor():
                self.transitionTo(self.S6_CONT_END)    
        elif self.state==self.S6_CONT_END:
            # run state 4 (chhose to continue or end) code
            self.transitionTo(self.S7_W_l_REC)   # Updating state for next iteration
            
        elif self.state==self.S7_W_l_REC:
            print('Wins:'+ str(self.W) + ' Losses:' + 'Win Rate:' + str(self.W/(self.L+self.W)))
            self.transitionTo(self.S0_INIT)   # Updating state for next iteration
        else:
            pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
    
    def pattern_match(self):
        #still need to implement way to only define self.dur once buttons are 
        #and not use old button values, also need to figure out how to store a 0
        #if its been more than a second and no button has been pressed 
        self.dur = utime.ticksdiff(self.ButtonEnd(),self.ButtonStart())
        if self.dur<200:
            self.game_pat.append(1)
        elif self.dur>= 200:
            self.game_pat.append(2)
        elif self.game_pat == self.pattern:
            return True
                
    def light_play(self,pattern_pt):
        lightT = utime.ticks_us()
        if pattern_pt == 0:
            pass
        elif pattern_pt == 1:
            self.pinA5.high()
            if 1000000* utime.ticksdiff(lightT, self.lightglob)>=0.2:
                self.pinA5.low()
                self.light_run+= 1
                self.lightglob = utime.ticks_us()
        elif pattern_pt == 2:
            self.pinA5.high()
            if 1000000* utime.ticksdiff(lightT, self.lightglob)>=0.6:
                self.pinA5.low()
                self.light_run+= 1
                self.lightglob = utime.ticks_us()
        elif self.light_run == len(self.pattern):
            self.light_done = True
             
    def win(self):
        pass
    
    def ButtonStart(self,IRQ_src):
        '''@brief Interrupt callback function; Captures initial button press
        @param IRQ_src Interrupt argument
        '''
        return utime.ticks_ms()
    def ButtonEnd(self, IRQ_src):
        '''@brief Interrupt callback function; Captures end of button press
        @param IRQ_src Interrupt argument
        '''
        return utime.ticks_ms()
