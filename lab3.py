'''@file        lab3.py
@brief          Simon says challenge
@details        Implements a finite state machine, shown in Aaron_Says.py to
                simulate the behavior of blinking LED patterns. Each round of 
                the game begins with a randomized sequence of blinks which 
                are randomly generated and somewhat resemble Morse Code.
                Players are to replicate the patterns played by the LED.
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/lab3.py
@author         Aaron Tran
                Christopher Tan
@date           2/18/21
@copyright      License Info Here
'''

from Aaron_Says import Simon

if __name__ == "__main__":
    ##@brief constructs game
    game= Simon()
    while True:
        try:
            game.run()
        except KeyboardInterrupt:
            print('Ctrl-c has been pressed')
            break
        