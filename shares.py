# -*- coding: utf-8 -*-
'''@file        shares.py
@brief          Allows for data to be shared between running tasks. This file should be imported into all files on the backend side.
@details        This file is also used to provide commands and user inputs. For instance, CTRL_task.py will kill the motor if kill=True and turn the motor back on if revive = True
                It is also necessary for UI_task.UI_task to tell CTRL_task.CTRL_task that the user has asked to zero the encoders, asked for position of encoders, or asked for latest delta values.
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/shares.py
@author         Aaron Tran
@date           3/17/21
'''


##@brief current encoder 1 position
enc1_pos = 0
##@brief current encoder 2 position
enc2_pos = 0
##@brief proportional gain value
Kp = 0.1937
##@brief integral gain value
Ki = 57.46
##@brief UI task is asking for delta value
get_delta = None
##@brief UI task has received delta value
UI_delta = None
##@brief UI task is asking to zero encoder
zero_enc= None
##@brief UI task is asking to turn motor off
kill = None
##@brief UI task is asking to turn motor on
revive = None
##@brief current encoder 1 speed
enc1_speed = 0
##@brief current encoder 2 speed
enc2_speed = 0
##@brief motor in use
motor = 2
##@brief motor 1 error
enc1_err = 0
##@brief motor 2 error
enc2_err = 0
