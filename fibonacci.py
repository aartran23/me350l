'''@file       fibonacci.py
@brief         Calculates the nth number in the fibonacci sequence
@author        Aaron Tran

@date          1/20/21

@details       Source code can be found at 
                    https://bitbucket.org/aartran23/me350l/src/master/fibonacci.py    
'''

def fib (idx):
    '''@brief       This function calculates a Fibonacci number at a specific 
                        index.
    @param          idx An integer specifying the index of the desired 
                        Fibonacci number
    ''' 
    fib = [0, 1]
    for n in range(2,idx+1):
        fib.append(fib[n-1] + fib[n-2])
    return fib[-1]
    


if __name__ == '__main__':
     while True:
         try:
             ##     Chosen Fibonacci Index
             idx  = int(input('Choose your desired Fibonacci number index:'))
             if idx<0:
                 z #Forces error if negative number chosen leading to except block
             else:
                 print ('Fibonacci number at ''index {:} is {:}.'.format(idx,fib(idx)))
             ##     Choose to run again or stop calculator     
             choice = input('Enter 1 to retrieve another and anything else to exit:')
             if choice == '1':
                pass
             else:
                break
         except: 
             print('Make sure the number you input is a nonnegative integer')

