# -*- coding: utf-8 -*-
'''@file        CTRL.py
@brief          Controls the motor speed and encoder position. This class and its methods are intended to be used and called by CTRL_task.py.
@details        Makes use of control systems theory to modulate motor speed and encoder position. This class and its methods are intended to be used and called by CTRL_task.py.
                For reference, the hand calculations have been included in this documentation once again. To tune the motor, change the values of Kp and Ki.
                Source code can be found at https://bitbucket.org/aartran23/me350l/src/master/CTRL.py
@image          html ctrl1.jpg
@image          html ctrl2.jpg
@image          html ctrl3.jpg
@image          html ctrl4.jpg "Figure 1. Hand calculations for implementing and determining appropriate gain values, Kp and Ki"
@author         Aaron Tran
@date           3/17/21
'''

class ClosedLoop:
    '''Implements a closed loop controller class for the ME405 board. Contains methods to retrieve and set proportional gain as well as run controller.
    '''
    def __init__(self):
        '''Creates ClosedLoop controller class
        '''
        ##@brief proportional gain
        self.Kp = 0.0203
        ##@brief integral gain
        self.Ki = 1.003
    def run(self,omega_ref, omega_meas, theta_ref, theta_meas):
        '''@brief Controls for motor speed and position
        @param omega_ref reference motor speed
        @param omega_meas measured motor speed
        @param theta_ref reference motor position
        @param theta_meas measured motor position
        '''
        ##@brief PWM of motor
        self.PWM = self.Kp*(omega_ref - omega_meas) + self.Ki*(theta_ref - theta_meas)
        if self.PWM >100:
            self.PWM = 100
        elif self.PWM <-100:
            self.PWM = -100
    
    def get_Kp(self):
        '''@brief returns proportional gain value
        '''
        return self.Kp
        pass
    
    def set_Kp(self, val):
        '''@brief sets value of proprotional gain
        '''
        self.Kp = val
        pass
    
